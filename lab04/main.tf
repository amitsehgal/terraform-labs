module "first" {
  source = "./module/s3/"
  bucket_name_prefix = "state-bucket"
}

# module "ec2_one" {
#  source = "./module/ec2/"
#  image_id = "ami-09d95fab7fff3776c"
#  env_type = "dev"
#  ec2_tags = {
#    Name = "ec2_one"
#    CreatedBy = "ModuleCall1"
#  }
# }

# module "ec2_two" {
#  source = "./module/ec2/"
#  image_id = "ami-09d95fab7fff3776c"
#  instance_type = "t2.micro"
#  env_type = "tst"
#  key_name = "my-key"
#  ec2_tags = {
#    Name = "ec2_two"
#    CreatedBy = "ModuleCall2"
#  }
# }

# module "second" {
#   source = "./module/s3/"
#   bucket_name_prefix = "mysecondmodbkt"
# }

# module "third" {
#   source = "./module/s3/"
#   bucket_name_prefix = "mythirdmodbkt"
# }