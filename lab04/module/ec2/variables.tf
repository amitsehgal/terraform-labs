variable "image_id" {
  description = "This variable contains image id"
  type        = string
}

variable "instance_type" {
  type        = string
  description = "This is my instanc type"
  default     = "t2.micro"
}

variable "key_name" {
  type        = string
  description = "Name of ssh key"
  default     = "my-key"
}

variable "ec2_tags" {
  type = map
}

variable "env_type" {
  type    = string
}