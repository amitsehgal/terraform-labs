resource "aws_s3_bucket" "b" {
  bucket = "myziyotek-training-${data.aws_caller_identity.current.account_id}-${data.aws_region.current.name}"
  acl    = "private"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}

resource "aws_s3_bucket_policy" "b" {
  bucket = aws_s3_bucket.b.id

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "MYBUCKETPOLICY",
  "Statement": [
    {
      "Sid": "AllowFromEc2",
      "Effect": "Allow",
      "Principal": {
          "AWS": "${aws_iam_role.test_role.arn}"
        },
      "Action": "s3:*",
      "Resource": [
        "${aws_s3_bucket.b.arn}/*",
        "${aws_s3_bucket.b.arn}"
        ]
    }
  ]
}
POLICY
}